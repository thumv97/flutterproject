import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutterofme/res/theme.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {



  @override
  void initState() {
    super.initState();
    startSplashScreen();
  }



  startSplashScreen() async {
    var duration = const Duration(seconds: 5);
    return Timer(duration, () {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_) {
          return HomePage();
        }),
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFFFFF),
      body: Center(
           child: Image.asset(
            'assets/images/logo.png',
            fit: BoxFit.scaleDown,
        ),
         ),
    );
  }
}
