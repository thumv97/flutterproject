import 'package:flutter/material.dart';
import 'file:///C:/Users/Admin/Desktop/Project_Me/flutterproject/flutter_of_me/lib/ui/res/colors.dart';
import 'file:///C:/Users/Admin/Desktop/Project_Me/flutterproject/flutter_of_me/lib/ui/res/theme.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.fromLTRB(30.0, 50.0, 30.0, 0),
        height: MediaQuery.of(context).size.height - 100,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                      width: 50,
                      height: 50,
                      child: Image(
                        image: AssetImage(
                            'assets/images/logo.png'),
                        fit: BoxFit.fill,
                      )),
                  SizedBox(
                    height: 10.0,
                  ),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: "Welcome,\n",
                          style: TextStyle(
                              fontFamily: 'SanProBold',
                              color: Colors.grey[500],
                              fontSize: 27),
                        ),
                        TextSpan(
                          text: 'sign up to continue',
                          style: Theme.of(context)
                              .textTheme
                              .headline5,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 7,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 35,top: 20),
                    child: TextField(
                      style:
                      Theme.of(context).textTheme.bodyText2,
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide:
                          BorderSide(color: Colors.black.withOpacity(.2)),
                        ),
                        hintText: "Email Address",
                        hintStyle: Theme.of(context)
                            .textTheme
                            .bodyText1,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 35),
                    child: TextField(
                      style:
                      Theme.of(context).textTheme.bodyText2,
                      decoration: InputDecoration(
                        hintText: "Password",
                        hintStyle:
                        Theme.of(context).textTheme.bodyText1,
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.black.withOpacity(.2),),
                        ),
                      ),
                    ),
                  ),
                  TextField(
                    style:
                    Theme.of(context).textTheme.bodyText2,
                    decoration: InputDecoration(
                      hintText: "Confirm Password",
                      hintStyle:
                      Theme.of(context).textTheme.bodyText1,
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black.withOpacity(.2),),
                      ),
                    ),
                  ),

                  Center(
                    child: FittedBox(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return null;
                                },
                              ));
                        },
                        child: Container(
                          width: 200,
                          margin: EdgeInsets.only(
                              bottom: 25, top: 40),
                          padding: EdgeInsets.symmetric(
                              horizontal: 26, vertical: 16),
                          decoration: BoxDecoration(
                            gradient: primaryGradient,
                            borderRadius:
                            BorderRadius.circular(25),
                            color: kPrimaryColor,
                          ),
                          child: Text("REGISTER",
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .textTheme
                                  .button
                                  .copyWith(
                                  color: Colors.black)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
