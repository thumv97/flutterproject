import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'file:///C:/Users/Admin/Desktop/Project_Me/flutterproject/flutter_of_me/lib/ui/res/colors.dart';
import 'file:///C:/Users/Admin/Desktop/Project_Me/flutterproject/flutter_of_me/lib/ui/pages/login_screen.dart';
import 'file:///C:/Users/Admin/Desktop/Project_Me/flutterproject/flutter_of_me/lib/ui/pages/register_screen.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Scaffold(
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 20.0, left: 20.0),
                width: 250,
                child: TabBar(
                    tabs: [
                      Tab(
                          icon: Text(
                        'Sign In',
                        style: Theme.of(context).textTheme.headline6,
                      )),
                      Tab(
                          icon: Text(
                        "Sign Up",
                        style: Theme.of(context).textTheme.headline6,
                      )),
                    ],
                    indicatorColor: kPrimaryColor,
                    indicatorSize: TabBarIndicatorSize.label),
              ),
              Expanded(
                child: TabBarView(
                  children: <Widget>[
                    SignInPage(),
                    SignUpPage(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
