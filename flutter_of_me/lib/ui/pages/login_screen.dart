import 'package:flutter/material.dart';
import 'file:///C:/Users/Admin/Desktop/Project_Me/flutterproject/flutter_of_me/lib/ui/res/colors.dart';
import 'file:///C:/Users/Admin/Desktop/Project_Me/flutterproject/flutter_of_me/lib/ui/res/theme.dart';

class SignInPage extends StatefulWidget {

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.fromLTRB(30.0, 50.0, 30.0, 0),
        height: MediaQuery.of(context).size.height - 100,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                      width: 50,
                      height: 50,
                      child: Image(
                        image: AssetImage(
                            'assets/images/logo.png'),
                        fit: BoxFit.fill,
                      )),
                  SizedBox(
                    height: 10.0,
                  ),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: "Welcome,\n",
                          style: TextStyle(
                              fontFamily: 'SanProBold',
                              color: Colors.grey[400],
                              fontSize: 27),
                        ),
                        TextSpan(
                          text: 'sign in to continue',
                          style: Theme.of(context)
                              .textTheme
                              .headline5,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 7,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 35, top: 20,),
                    child: TextField(
                      style:
                      Theme.of(context).textTheme.bodyText2,
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide:
                          BorderSide(color: Colors.black.withOpacity(.2)),
                        ),
                        hintText: "Email Address",
                        hintStyle: Theme.of(context)
                            .textTheme
                            .bodyText1,
                      ),
                    ),
                  ),
                  TextField(
                    style:
                    Theme.of(context).textTheme.bodyText2,
                    decoration: InputDecoration(
                      hintText: "Password",
                      hintStyle:
                      Theme.of(context).textTheme.bodyText1,
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black.withOpacity(.2),),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    'Forgot Pasword',
                    style:
                    Theme.of(context).textTheme.bodyText2,
                  ),
                  Center(
                    child: FittedBox(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return null;
                                },
                              ));
                        },
                        child: Container(
                          width: 200,
                          margin: EdgeInsets.only(
                              bottom: 25, top: 25),
                          padding: EdgeInsets.symmetric(
                              horizontal: 26, vertical: 16),
                          decoration: BoxDecoration(
                            gradient: primaryGradient,
                            borderRadius:
                            BorderRadius.circular(25),
                            color: kPrimaryColor,
                          ),
                          child: Text("LOGIN",
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .textTheme
                                  .button
                                  .copyWith(
                                  color: Colors.black)),
                        ),
                      ),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Divider(
                          color: Colors.black,
                          indent: 50,
                          endIndent: 10,
                          thickness: 1,
                        ),
                      ),
                      Text('OR'),
                      Expanded(
                        child: Divider(
                          indent: 10,
                          endIndent: 50,
                          color: Colors.black,
                          thickness: 1,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20,bottom: 30),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(16),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color:
                              Colors.black.withOpacity(.5),
                            ),
                          ),
                          child: Image(
                            width: 30,
                            height: 30,
                            image: AssetImage('assets/images/logo_fb.png'),
                            fit: BoxFit.fill,
                          ),
                        ),
                        SizedBox(width: 20),
                        Container(
                          padding: EdgeInsets.all(16),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color:
                              Colors.black.withOpacity(.5),
                            ),
                          ),
                          child: Image(
                            width: 30,
                            height: 30,
                            image: AssetImage('assets/images/logo_gg.png'),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
