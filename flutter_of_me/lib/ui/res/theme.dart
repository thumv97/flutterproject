import 'package:flutter/material.dart';


const Color loginGradientStart = const Color(0xFFb8dbff);
const Color loginGradientEnd = const Color(0xFF999dff);

const primaryGradient = const LinearGradient(
    colors: const [loginGradientStart, loginGradientEnd],
    stops: const [0.0, 1.0],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
   );