import 'dart:ui';

const kBackgroundColor = Color(0xFFFFFFFF);
const kPrimaryColor = Color(0xFF3399ff);
const kFontColor = Color(0xFF202020);