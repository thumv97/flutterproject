import 'package:acazia_training/navigation/paths.dart';
import 'package:acazia_training/navigation/slide_left_route.dart';
import 'package:acazia_training/view/pages/home_page.dart';
import 'package:acazia_training/view/pages/signin_page.dart';
import 'package:acazia_training/view/pages/signup/signup_acount.dart';
import 'package:acazia_training/view/pages/signup/signup_created.dart';
import 'package:acazia_training/view/pages/signup/signup_page.dart';
import 'package:acazia_training/view/pages/signup/signup_phone.dart';
import 'package:acazia_training/view/pages/signup/signup_verifycode.dart';
import 'package:flutter/material.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case HOME:
      final user = settings.arguments;
      return SlideLeftRoute(
          widget: MyHomePage(
        user: user,
      ));
    case SIGNINPAGE:
      return SlideLeftRoute(widget: SignInPage());
    case SIGNUP:
      return SlideLeftRoute(widget: SignUpPage());
    case SIGNUP_PHONE:
      return SlideLeftRoute(widget: SignUpPhone());
    case SIGNUP_VERIFY:
      final String phoneNumberVerify = settings.arguments;
      return SlideLeftRoute(
          widget: SignUpVerify(
        phoneNumberVerify: phoneNumberVerify,
      ));
    case SIGNUP_ACOUNT:
      final user = settings.arguments;
      return SlideLeftRoute(widget: SignUpEmailPage(user: user));
    case SIGNUP_CREATED:
      return SlideLeftRoute(widget: SignUpCreated());
    default:
      return defaultNav(settings);
  }
}

MaterialPageRoute defaultNav(RouteSettings settings) {
  return MaterialPageRoute(
    builder: (context) => Scaffold(
      backgroundColor: Colors.grey,
      body: Center(
        child: Text('No path for ${settings.name}'),
      ),
    ),
  );
}
